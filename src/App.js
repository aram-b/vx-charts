import React from 'react';
import './App.css';
import Chart from './components/VX/Chart'
import XAxis from './components/VX/XAxis'
import YAxis from './components/VX/YAxis'
import Line from './components/VX/Line'
import Legend from './components/VX/Legend'
import Tooltip from './components/VX/Tooltip'
import Grid from './components/VX/Grid'

/*
  TODO:
  - grid rows
  - tooltip voor meerdere lijnen
  - label
  - kleurtjes
  - y is date of cat
*/

const data1 = [
  {
    name: '2010-06-11T07:00:00.000Z', uv: 4000, pv: 2400, amt: 2400,
  },
  {
    name: '2010-06-14T07:00:00.000Z', uv: 3000, pv: 1398, amt: 2210,
  },
  {
    name: '2010-06-15T07:00:00.000Z', uv: 2000, pv: 9800, amt: 2290,
  },
  {
    name: '2010-06-16T07:00:00.000Z', uv: 2780, pv: 3908, amt: 2000,
  },
  {
    name: '2010-06-17T07:00:00.000Z', uv: 1890, pv: 4800, amt: 2181,
  },
  {
    name: '2010-06-18T07:00:00.000Z', uv: 2390, pv: 3800, amt: 2500,
  },
  {
    name: '2010-06-21T07:00:00.000Z', uv: 3490, pv: 4300, amt: 2100,
  },
];

function App() {
  return (
    <div className="App">
      <Chart
        data={data1}
        xKey='name'
        yKey='pv'
      >
        <Legend />
        <Line dataKey='pv' />
        <XAxis />
        <YAxis />
        <Grid />
        <Tooltip />
      </Chart>
    </div>
  );
}

export default App;
