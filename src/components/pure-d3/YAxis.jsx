import React from 'react'
import * as d3 from 'd3'

const YAxis = ({ height, margin, data, dataKey, tickHeight = 60 }) => {
  const yScale = d3.scaleLinear()
    .domain([0, d3.max(data, d => d[dataKey])]).nice()
    .range([height-margin.bottom, margin.top]);

  const ticks = yScale.ticks(height / tickHeight).length > 2 ?
  [...new Set([...yScale.ticks(height / tickHeight) ,...yScale.domain()])] :
  yScale.domain()

  return (
    <>
      <line
        x1={margin.left}
        x2={margin.left}
        y1={height-margin.bottom}
        y2={margin.top}
        style={{stroke:'black', strokeWidth:2}}
      />
      {ticks.map((d, i) => (
        <text
          key={i}
          style={{ textAnchor: "middle", fontSize: 12 }}
          dx={-12}
          dy={4}
          x={margin.left}
          y={yScale(d)}
        >
          {d}
        </text>
      ))}
    </>
  )
}

export default YAxis