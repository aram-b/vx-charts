import React from 'react'
import * as d3 from 'd3'

const Line = ({ height, width, margin, data, xKey, yKey }) => {

  const x = d3.scalePoint()
    .domain(data.map(d => d[xKey]))
    .range([margin.left, width - margin.right]);

  const y = d3.scaleLinear()
    .domain([0, d3.max(data, d => d[yKey])]).nice()
    .range([height - margin.bottom, margin.top]);

  return (
    <path 
      fill="none"
      stroke="#33C7FF"
      strokeWidth="2"
      d={d3.line().x(d => x(d[xKey])).y(d => y(d[yKey]))(data)} 
    />
  )
}

export default Line