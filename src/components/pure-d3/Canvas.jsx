import React from 'react'

const Canvas = ({ width, height, margin}) => (
  <rect
    width={width-margin.left-margin.right}
    height={height-margin.top-margin.bottom}
    style={{ transform: `translate(${margin.left}px, ${margin.top}px)`, fill: 'green' }}
  />
)

export default Canvas