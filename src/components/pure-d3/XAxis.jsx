import React from 'react'
import * as d3 from 'd3'

const XAxis = ({ height, width, margin, data, dataKey, tickWidth = 100 }) => {
  const xScale = d3.scalePoint()
    .domain(data.map(d => d[dataKey]))
    .range([margin.left, width-margin.right]);

  const modulo = width / tickWidth > xScale.domain().length ? 1 : Math.ceil(xScale.domain().length / (width / tickWidth))
  const ticks = xScale.domain().filter((d,i) => !(i % modulo))

  return (
    <>
      <line
        x1={margin.left}
        x2={width-margin.right}
        y1={height-margin.bottom}
        y2={height-margin.bottom}
        style={{stroke:'black', strokeWidth:2}}
      />
      {ticks.map((d, i) => (
        <text
          key={i}
          style={{ textAnchor: "middle", fontSize: 12 }}
          dy={20}
          x={xScale(d)}
          y={height - margin.bottom}
        >
          {d}
        </text>
      ))}
    </>
  )
}

export default XAxis