import React, { useRef, useEffect, useState } from 'react'

const Chart = ({ margin={ top: 0, right: 20, bottom: 24, left: 20 }, data, children }) => {
  
  const containerRef = useRef()
  const [dimensions, setDimensions] = useState({
    height: 0, 
    width: 0
  })

  useEffect(() => {
    if (containerRef.current) {
      function handleResize () {
        setDimensions({
          height: containerRef.current.offsetHeight,
          width: containerRef.current.offsetWidth
        })
      }
      // onload
      handleResize()
      //onresize
      window.addEventListener('resize', handleResize)
    }
  }, [containerRef])

  return (
    <div style={{ height: '100%', width: '100%' }} ref={containerRef}>
      <svg
        width={dimensions.width}
        height={dimensions.height}
        style={{ backgroundColor: 'aliceblue' }}
      >
        {React.Children.map(children, child =>
          React.cloneElement(child, { margin, data, height: dimensions.height, width: dimensions.width })
        )}
      </svg>
    </div>
  )
}

export default Chart