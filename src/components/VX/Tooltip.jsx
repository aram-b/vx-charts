import React from 'react'
import { Line } from '@vx/shape';

const Tooltip2 = ({ parent, margin, handleTooltip, hideTooltip, tooltipData, tooltipLeft, tooltipTop }) => {
  return (
    <>
      {parent.ref && <rect 
        width={parent.width-margin.left-margin.right}
        height={parent.height-margin.top-margin.bottom}
        x={margin.left}
        y={margin.top}
        style={{ fill: 'transparent' }}
        onMouseMove={handleTooltip}
        onMouseLeave={() => hideTooltip()}
      />}
      {tooltipData && (
        <g>
          <Line
            from={{ x: tooltipLeft, y: margin.top }}
            to={{ x: tooltipLeft, y: parent.height-margin.bottom }}
            stroke='black'
            strokeWidth={2}
            pointerEvents="none"
            strokeDasharray="5,2"
          />
          <circle
            cx={tooltipLeft}
            cy={tooltipTop + 1}
            r={4}
            fill="black"
            fillOpacity={0.1}
            stroke="black"
            strokeOpacity={0.1}
            strokeWidth={2}
            pointerEvents="none"
          />
          <circle
            cx={tooltipLeft}
            cy={tooltipTop}
            r={4}
            fill='black'
            stroke="white"
            strokeWidth={2}
            pointerEvents="none"
          />
        </g>
      )}
    </>
  )
}

export default Tooltip2