import React from 'react'
import { LegendOrdinal } from '@vx/legend';

const Legend = ({ zScale, parent, margin }) => (
  <div
    style={{
      position: "absolute",
      top: margin.top / 2 - 10,
      left: margin.left,
      width: parent.width-margin.left,
      display: "flex",
      justifyContent: "start",
      fontSize: "14px"
    }}
    >
    <LegendOrdinal
      scale={zScale}
      direction="row"
      labelMargin="0 15px 0 0"
      shape='line'
    />
  </div>
)

export default Legend