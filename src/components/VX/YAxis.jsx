import React from 'react'
import { AxisLeft } from '@vx/axis'

const YAxis = ({ yScale, margin }) => (
  <AxisLeft 
    scale={yScale}
    left={margin.left}
  />
)

export default YAxis