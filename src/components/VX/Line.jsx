import React from 'react'
import { LinePath } from "@vx/shape";

const Line = ({ xScale, xKey, yScale, data, dataKey }) => {
  return (
    <LinePath
      data={data}
      x={d => xScale(new Date(d[xKey]))}
      y={d => yScale(d[dataKey])}
      strokeWidth={2}
      stroke='black'
    />
  )
}

export default Line