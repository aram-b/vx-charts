import React, { useCallback } from 'react';
import { ParentSize } from '@vx/responsive';
import { scaleTime, scaleLinear, scaleOrdinal } from "@vx/scale";
import { localPoint } from '@vx/event';
import { useTooltip, TooltipWithBounds } from '@vx/tooltip';
import { extent, max } from "d3-array";
import { bisector } from 'd3-array';

const margin = {
  top: 40, 
  bottom: 30,
  left: 50,
  right: 0
}

const Chart = ({ children, data, xKey, yKey }) => {
  const xSelector = useCallback(d => new Date(d[xKey]), [xKey]);
  const ySelector = useCallback(d => d[yKey], [yKey]);
  const bisectDate = bisector(d => xSelector(d)).left;
  const yMax = max(data, ySelector);

  let xScale, yScale

  // tooltip
  const {
    tooltipData,
    tooltipLeft,
    tooltipTop,
    showTooltip,
    hideTooltip,
  } = useTooltip();

  // tooltip handler
  const handleTooltip = useCallback(event => {
    const { x } = localPoint(event) || { x: 0 };
    const x0 = xScale.invert(x);
    const index = bisectDate(data, x0, 1);
    const d0 = data[index - 1];
    const d1 = data[index];
    let d = d0;
    if (d1 && xSelector(d1)) {
      d = x0.valueOf() - xSelector(d0).valueOf() > xSelector(d1).valueOf() - x0.valueOf() ? d1 : d0;
    }
    showTooltip({
      tooltipData: d,
      tooltipLeft: xScale(xSelector(d)),
      tooltipTop: yScale(ySelector(d)),
    });
  }, [showTooltip, yScale, xScale, data, xSelector, ySelector, bisectDate])

  return (
    <div style={{ width: '100%', height: '100%', position: 'relative' }}>
      <ParentSize debounceTime={30}>
        {parent => {
          // xScale
          xScale = scaleTime({
            range: [margin.left, parent.width-margin.right],
            domain: extent(data, xSelector),
          }); // heeft useMemo voor scales zin?
          // yScale
          yScale = scaleLinear({
            range: [parent.height-margin.bottom, margin.top],
            domain: [0, yMax + (yMax / 4)],
          });
          // legend
          const legendDomain = new Set()
          React.Children.forEach(children, element => {
            if (!React.isValidElement(element)) return
            const { dataKey } = element.props
            if (dataKey) {
              legendDomain.add(dataKey)
            }
          })
          const zScale = scaleOrdinal({
            domain: [...legendDomain],
            range: ["black"]
          })
          return (
            <>
              {React.Children.map(children, child =>
                child && child.type.name === 'Legend' ? React.cloneElement(child, { parent, margin, zScale }) : null
              )}
              <svg height={parent.height} width={parent.width}>
                {React.Children.map(children, child =>
                  child && child.type.name !== 'Legend' ? React.cloneElement(child, { parent, margin, data, xScale, yScale, xKey, yKey, handleTooltip, hideTooltip, tooltipData, tooltipLeft, tooltipTop }) : null
                )}
              </svg>
              {tooltipData && 
                <TooltipWithBounds
                  // set this to random so it correctly updates with parent bounds
                  key={Math.random()}
                  top={tooltipTop}
                  left={tooltipLeft}
                >
                  {tooltipData.pv}
                </TooltipWithBounds>
              }
            </>
          )
        }}
      </ParentSize>
    </div>
  )
}

export default Chart