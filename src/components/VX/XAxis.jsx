import React from 'react'
import { AxisBottom } from '@vx/axis'

const XAxis = ({ xScale, margin, parent }) => (
  <AxisBottom 
    scale={xScale}
    top={parent.height-margin.bottom}
  />
)

export default XAxis