import React from 'react'
import { Grid } from '@vx/grid';

const Grid1 = ({ margin, xScale, yScale, parent }) => (
  <Grid
    top={margin.top}
    left={margin.left}
    xScale={xScale}
    yScale={yScale}
    width={parent.width-margin.left-margin.right}
    height={parent.height-margin.bottom}
    stroke="black"
    strokeOpacity={0.1}
    numTicksRows={12}
    numTicksColumns={10}
    //xOffset={xScale.bandwidth() / 2}
  />
)

export default Grid1